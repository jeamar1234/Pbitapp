// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('Pbit', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

  
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
      StatusBar.hide();
    }

      // if(window.localStorage.getItem("user_session") == "true"){
      //   console.log('getLoginStatus', "Success");

      //   $scope.user_id = window.localStorage.getItem("user_id");  
      //   $scope.user_name = window.localStorage.getItem("user_name");
      //   $scope.user_email = window.localStorage.getItem("user_email");
      //   $scope.user_pic = window.localStorage.getItem("user_pic");

      //   $scope.logUserIn();
      // }
  });
})

.directive('noScroll', function() {
    return {
        restrict: 'A',
        link: function($scope, $element, $attr) {
            $element.on('touchmove', function(e) {
                e.preventDefault();
            });
        }
    }
})

.config(function($stateProvider, $urlRouterProvider,$compileProvider,$ionicConfigProvider) {

  // remove back button text completely
  $ionicConfigProvider.backButton.previousTitleText(false).text('');

  $stateProvider 
   .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })
    .state('header', {
      url: '/header',
      abstract: true,
      templateUrl: 'templates/header.html'
    })
    .state('home', {
      url: "/home",
      parent: "header",
      views: {
        'content': {
          templateUrl: "templates/home.html",
        }
      }
    })

    .state('pics', {
      url: "/pics",
      parent: "header",
      views: {
        'content': {
          templateUrl: "templates/pics.html",
        }
      }
    })

    $urlRouterProvider.otherwise('/login');

})


.directive('noScroll', function($document) {

  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

      $document.on('touchmove', function(e) {
        e.preventDefault();
      });
    }
  }
})

.controller('Pbitctrl', function($scope, $state, $timeout) {
  
  angular.element(document).ready(function () {
      setTimeout(function(){
        $scope.hideLoad();
        $('.login').show();
      },1500);
  });

  $scope.$on('$ionicView.loaded', function() {
    ionic.Platform.ready( function() {
      if(navigator && navigator.splashscreen) navigator.splashscreen.hide();
    });
  });


  $scope.logUserIn = function() {
    $('.login').hide();
    $scope.showLoad();
    setTimeout(function(){
        $state.go('home');
        $scope.hideLoad();
        $('.login').show();
    },1500);  
  }

  $scope.GoIn = function() {
    $('.login').hide();
    $scope.showLoad();
    $scope.user = 'User'
    $scope.user_pic = 'img/user.png'
    setTimeout(function(){
        $state.go('home');
        $scope.hideLoad();
        $('.login').show();
    },1500);  
  }


  $scope.setUserData = function(data) {
    $scope.user_name = data.name;
    $scope.user_pic = "https://graph.facebook.com/" + data.id + "/picture?width=270&height=270";
    
    window.localStorage.setItem("user_session","true");
    window.localStorage.setItem("user_id",data.id);
    window.localStorage.setItem("user_name",data.name);
    window.localStorage.setItem("user_email",data.email);
    window.localStorage.setItem("user_pic",$scope.user_pic);
    console.log(data);
  };

  $scope.getFacebookProfileInfo = function (authResponse) {
    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
        console.log(response);
        $scope.setUserData(response);
        $scope.logUserIn(); 
      },
      function (response) {
        console.log(response);
        console.log('profile info fail', fail);
      }
    );
  };

  $scope.logUserOut = function() {
    console.log("Logout");
    facebookConnectPlugin.logout(function(){
      $state.go('login');
      window.localStorage.setItem("user_session","false");
    });
    $('.login').hide();
    $scope.showLoad();
    setTimeout(function(){
        $scope.hideLoad();
        $('.login').show();
    },2000); 
  }

  $scope.fbLogin = function ($facebookConnectPlugin) {
    console.log("Login");
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        console.log('getLoginStatus', success.status);

        $scope.user_id = window.localStorage.getItem("user_id");  
        $scope.user_name = window.localStorage.getItem("user_name");
        $scope.user_email = window.localStorage.getItem("user_email");
        $scope.user_pic = window.localStorage.getItem("user_pic");

        $scope.logUserIn();
      }else{
        console.log('getLoginStatus', success.status);
        facebookConnectPlugin.login(["public_profile,email"], 
          function (response) { 
            var authResponse = response.authResponse;
            console.log("LogIn Success");
            $scope.getFacebookProfileInfo(authResponse);
            
          },
          function (error) { 
            console.log('fbLoginError', error);
          });
      }
    });
  };


  $scope.hideLoad = function () {
      $('.loading').hide();
  };
  $scope.showLoad = function () {
      $('.loading').show();
  };




  var cards = [
        { image: 'img/happiness4.jpg' },
        { image: 'img/happiness1.jpg' },
        { image: 'img/happiness2.jpg' }
      ];

  var temp_cards = cards; 

  
  

  $scope.cards = {
    // Master - cards that haven't been discarded
    master: Array.prototype.slice.call(cards, 0),
    // Active - cards displayed on screen
    active: Array.prototype.slice.call(cards, 0),
    // Discards - cards that have been discarded
    discards: [],
    // Liked - cards that have been liked
    liked: [],
    // Disliked - cards that have disliked
    disliked: []
  }

  $scope.survey = temp_cards.length;
  $scope.points = 0;
  $scope.liked = 0;
  $scope.unliked = 0;


  var count = temp_cards.length-1;

  $scope.nextbtn = function(){
    if(count != (temp_cards.length-1) ){
      setTimeout(function(){
        for( var x = 0; x <= count; x++ ){
          $(".image-container").append('<div class="image card-' + x + '"><img src="' + cards[x].image + '"></div>');
          console.log(cards[x].image);    
        }
        $("#img-load").hide();
        $("#end-card").show();
      },700);
    }else{
      setTimeout(function(){
        for( var x = 0; x < cards.length; x++ ){
          $(".image-container").append('<div class="image card-' + x + '"><img src="' + cards[x].image + '"></div>');
          console.log(cards[x].image);    
        }
        $("#img-load").hide();
        $("#end-card").show();
      },700);
    }
    
  }

  $scope.refreshCards = function() {
    // Set $scope.cards to null so that directive reloads
    temp_cards = null;
    $("#img-load").show();
    $("#end-card").hide();
    setTimeout(function() {
      for( var x = 0; x < cards.length; x++ ){
          $(".image-container").append('<div class="image card-' + x + '"><img src="' + cards[x].image + '"></div>');
          console.log(cards[x].image);    
        }
      temp_cards = cards;
      count = temp_cards.length-1;
      $scope.survey = temp_cards.length;
      $scope.points = 0;
      $scope.liked = 0;
      $scope.unliked = 0;
      $("#img-load").hide();
      $("#end-card").show();
    },700);
  }

  $scope.like = function(){
    if( count != -1 ){
      console.log("in");
      var interval = null;
      var counter = 0;
      var opacity = 1;
      var margin = 0;
      var $this = $(".card-" + count);
      clearInterval(interval);
   
      interval = setInterval(function(){
          if (counter != -360) {
              counter -= 5;
              opacity -= .02;
              margin -= 5;
              $this.css({
                  MozTransform: 'rotate(-' + -counter + 'deg)',
                  WebkitTransform: 'rotate(-' + -counter + 'deg)',
                  transform: 'rotate(-' + -counter + 'deg)',
                  opacity: opacity,
                  left: margin + 'px'

              });
          }else{
            $this.css({
                  display: 'none'
              });

          }
      }, 10);

      count--;
      $scope.survey--;
      $scope.points++;
      $scope.liked++;
      
    }
  }

  $scope.unlike = function(){
    if( count != -1 ){

      
      console.log("in");

      var interval = null;
      var counter = 0;
      var opacity = 1;
      var margin = 0;
      var $this = $(".card-" + count);
      clearInterval(interval);
   
      interval = setInterval(function(){
          if (counter != -360) {
              counter -= 5;
              opacity -= .02;
              margin -= 5;
              $this.css({
                  MozTransform: 'rotate(' + -counter + 'deg)',
                  WebkitTransform: 'rotate(' + -counter + 'deg)',
                  transform: 'rotate(' + -counter + 'deg)',
                  opacity: opacity,
                  right: margin + 'px'

              });
          }else{
            $this.css({
                  display: 'none'
              });
          }
      }, 10);
      
      count--;
      $scope.survey--;
      $scope.points++;
      $scope.unliked++;
    }
  }

})
